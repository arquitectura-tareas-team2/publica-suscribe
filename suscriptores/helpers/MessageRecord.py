#-------------------------------------------------------------------------
# Archivo: MessageRecord.py
# Capitulo: Estilo Publica-Suscribe
# Autor(es): 
#                 - Ulises Enrique Huerta Elías
#                 - Juan Francisco Navarro Ambriz
#                 - Alexis Ultreras Sotelo
#                 - Reyna Esmeralda Sánchez Rodríguez
# Version: 4.0.0 Mayo 2023
# Descripción:   
#
#   Este archivo nos permite obtener los mensajes recibidos del publicador, en el suscriptor Record.
#
#   A continuación se describen los métodos que se implementaron en esta clase:
#
#                                             Métodos:
#           +------------------------+--------------------------+-------------------------+
#           |         Nombre         |        Parámetros        |        Función          |
#           +------------------------+--------------------------+-------------------------+
#           |        on_message()    |  - self: definición de   |  - realiza la impresión |
#           |                        |    la instancia de la    |    de cuando los datos  |
#           |                        |    clase                 |    son correctos        |
#           |                        |  - message: mensaje que  |                         |
#           |                        |    mandara para hacer su |                         |
#           |                        |    impresión.            |                         |
#           +------------------------+--------------------------+-------------------------+
#


from helpers.Message import Message
import stomp, json, time

class MessageRecord(Message):
    def on_message(self, message):
        data = json.loads(message.body)
        print("datos recibidos, actualizando expediente del paciente...")
        record_file = open(f"./records/{data['ssn']}.txt", 'a')
        record_file.write(f"\n[{data['wearable']['date']}]: {data['name']} {data['last_name']}... ssn: {data['ssn']}, edad: {data['age']}, temperatura: {round(data['wearable']['temperature'], 1)}, ritmo cardiaco: {data['wearable']['heart_rate']}, presión arterial: {data['wearable']['blood_pressure']}, dispositivo: {data['wearable']['id']}")
        record_file.close()
        time.sleep(1)