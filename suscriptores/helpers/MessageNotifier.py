#-------------------------------------------------------------------------
# Archivo: MessageNotifier.py
# Capitulo: Estilo Publica-Suscribe
# Autor(es): 
#                 - Ulises Enrique Huerta Elías
#                 - Juan Francisco Navarro Ambriz
#                 - Alexis Ultreras Sotelo
#                 - Reyna Esmeralda Sánchez Rodríguez
# Version: 4.0.0 Mayo 2023
# Descripción:   
#
#   Este archivo nos permite obtener los mensajes recibidos del publicador, en el suscriptor Notifier.
#
#   A continuación se describen los métodos que se implementaron en esta clase:
#
#                                             Métodos:
#           +------------------------+--------------------------+-------------------------+
#           |         Nombre         |        Parámetros        |        Función          |
#           +------------------------+--------------------------+-------------------------+
#           |        on_message()    |  - self: definición de   |  - realiza la impresión |
#           |                        |    la instancia de la    |    de cuando los datos  |
#           |                        |    clase                 |    son correctos        |
#           |                        |  - message: mensaje que  |                         |
#           |                        |    mandara para hacer su |                         |
#           |                        |    impresión.            |                         |
#           +------------------------+--------------------------+-------------------------+
#


from helpers.Message import Message
import stomp, json, time

class MessageNotifier(Message):
    def on_message(self, message):
        data = json.loads(message.body)
        print(f"ADVERTENCIA!!!\n[{data['wearable']['date']}]: asistir al paciente {data['name']} {data['last_name']}...\nssn: {data['ssn']}, edad: {data['age']}, temperatura: {round(data['wearable']['temperature'], 1)}, ritmo cardiaco: {data['wearable']['heart_rate']}, presión arterial: {data['wearable']['blood_pressure']}, dispositivo: {data['wearable']['id']}")
