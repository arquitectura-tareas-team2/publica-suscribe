#-------------------------------------------------------------------------
# Archivo: Message.py
# Capitulo: Estilo Publica-Suscribe
# Autor(es): 
#                 - Ulises Enrique Huerta Elías
#                 - Juan Francisco Navarro Ambriz
#                 - Alexis Ultreras Sotelo
#                 - Reyna Esmeralda Sánchez Rodríguez
# Version: 4.0.0 Mayo 2023
# Descripción:
#
#   Este archivo nos permite obtener los mensajes recibidos del publicador.
#
#   A continuación se describen los métodos que se implementaron en esta clase:
#
#                                             Métodos:
#           +------------------------+--------------------------+-------------------------+
#           |         Nombre         |        Parámetros        |        Función          |
#           +------------------------+--------------------------+-------------------------+
#           |       __init__()       |  - self: definición de   |  - constructor de la    |
#           |                        |    la instancia de la    |    clase                |
#           |                        |    clase                 |                         |
#           |                        |  - tipo: definición de   |                         |
#           |                        |    la conexión para      |                         |
#           |                        |    el envio del mensaje  |                         |
#           +------------------------+--------------------------+-------------------------+
#           |       on_error()       |  - self: definición de   |  - realiza la           |
#           |                        |    la instancia de la    |    impresión de cuando  |
#           |                        |    clase                 |    los datos no son     |
#           |                        |  - message: mensaje que  |    correctos enviando   |
#           |                        |    mandará para hacer su |    un mensaje de error. |
#           |                        |    impresión.            |                         |
#           |                        |                          |                         |
#           +------------------------+--------------------------+-------------------------+
#           |        on_message()    |  - self: definición de   |  - realiza la impresión |
#           |                        |    la instancia de la    |    de cuando los datos  |
#           |                        |    clase                 |    son correctos.       |
#           |                        |  - message: mensaje que  |                         |
#           |                        |    mandará para hacer su |                         |
#           |                        |    impresión.            |                         |
#           +------------------------+--------------------------+-------------------------+
#

import stomp, json, time

class Message(stomp.ConnectionListener):
    def __init__(self):
        self.msg_received = 0

    def on_error(self, message):
        print("Error recibido: %s", message)

    def on_message(self, message):
        pass