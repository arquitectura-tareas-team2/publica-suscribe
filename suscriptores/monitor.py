##!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: monitor.py
# Capitulo: Estilo Publica-Suscribe
# Autor(es): Perla Velasco & Yonathan Mtz. & Jorge Solís
# Actualizado por:
#                 - Ulises Enrique Huerta Elías
#                 - Juan Francisco Navarro Ambriz
#                 - Alexis Ultreras Sotelo
#                 - Reyna Esmeralda Sánchez Rodríguez
# Version: 4.0.0 Mayo 2023
# Descripción:
#
#   Esta clase define el suscriptor que recibirá mensajes desde el distribuidor de mensajes
#   y los mostrará al área interesada para su monitoreo continuo
#
#   Este archivo también define el punto de ejecución del Suscriptor
#
#   A continuación se describen los métodos que se implementaron en esta clase:
#
#                                             Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |       __init__()       |  - self: definición de   |  - constructor de la  |
#           |                        |    la instancia de la    |    clase              |
#           |                        |    clase                 |                       |
#           +------------------------+--------------------------+-----------------------+
#           |       suscribe()       |  - self: definición de   |  - inicializa el      |
#           |                        |    la instancia de la    |    proceso de         |
#           |                        |    clase                 |    monitoreo de       |
#           |                        |                          |    signos vitales     |
#           +------------------------+--------------------------+-----------------------+
#           |        consume()       |  - self: definición de   |  - realiza la         |
#           |                        |    la instancia de la    |    suscripción en el  |
#           |                        |    clase                 |    distribuidor de    |
#           |                        |  - destination: ruta a la|    mensajes para      |
#           |                        |    que el suscriptor está|    comenzar a recibir |
#           |                        |    interesado en recibir |    mensajes           |
#           |                        |    mensajes              |                       |
#           +------------------------+--------------------------+-----------------------+
#
##-------------------------------------------------------------------------
import sys
import time, stomp

from helpers.MessageMonitor import MessageMonitor
    
class Monitor:
    def __init__(self):
        self.topic = "topic/monitor"

    def suscribe(self):
        print("Inicio de monitoreo de signos vitales...")
        print()
        self.consume(destination=self.topic)

    def consume(self, destination):
        try:
            conn = stomp.Connection([("localhost",61613)])
            listener = MessageMonitor()
            conn.set_listener("",listener)
            conn.connect("","",wait=True)
            while True:
                conn.subscribe(destination, header={}, id="suscriber", atc="client")
                time.sleep(1)
        except (KeyboardInterrupt, SystemExit):
            conn.disconnect()
            sys.exit("Conexión finalizada...")

        
if __name__ == '__main__':
    monitor = Monitor()
    monitor.suscribe()


