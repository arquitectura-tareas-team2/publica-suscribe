##!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: notifier.py
# Capitulo: Estilo Publica-Suscribe
# Autor(es): Perla Velasco & Yonathan Mtz. & Jorge Solís
# Actualizado por:
#                 - Ulises Enrique Huerta Elías
#                 - Juan Francisco Navarro Ambriz
#                 - Alexis Ultreras Sotelo
#                 - Reyna Esmeralda Sánchez Rodríguez
# Version: 4.0.0 Mayo 2023
# Descripción:
#
#   Esta clase define el suscriptor que recibirá mensajes desde el distribuidor de mensajes
#   y lo notificará a un(a) enfermero(a) én particular para la atención del adulto mayor en
#   cuestión
#
#   Este archivo también define el punto de ejecución del Suscriptor
#
#   A continuación se describen los métodos que se implementaron en esta clase:
#
#                                             Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |       __init__()       |  - self: definición de   |  - constructor de la  |
#           |                        |    la instancia de la    |    clase              |
#           |                        |    clase                 |                       |
#           +------------------------+--------------------------+-----------------------+
#           |       suscribe()       |  - self: definición de   |  - inicializa el      |
#           |                        |    la instancia de la    |    proceso de         |
#           |                        |    clase                 |    monitoreo de       |
#           |                        |                          |    signos vitales     |
#           +------------------------+--------------------------+-----------------------+
#           |        consume()       |  - self: definición de   |  - realiza la         |
#           |                        |    la instancia de la    |    suscripción en el  |
#           |                        |    clase                 |    distribuidor de    |
#           |                        |  - destination: ruta a la|    mensajes para      |
#           |                        |    que el suscriptor está|    comenzar a recibir |
#           |                        |    interesado en recibir |    mensajes           |
#           |                        |    mensajes              |                       |
#           +------------------------+--------------------------+-----------------------+
#

import sys, time, stomp

from helpers.MessageNotifier import MessageNotifier

class Notifier:

    def __init__(self):
        self.topic = "topic/notifier"

    def suscribe(self):
        print("Inicio de gestión de notificaciones...")
        print()
        self.consume(destination=self.topic)

    def consume(self, destination):
        try:
            conn = stomp.Connection([('localhost', 61613)])
            listener = MessageNotifier()
            conn.set_listener("", listener)
            conn.connect("","",wait=True)
            conn.subscribe(destination, header={}, id="suscriber", atc="client")
            while True:
                time.sleep(1)
        except (KeyboardInterrupt, SystemExit):
            conn.disconnect()
            sys.exit("Conexión finalizada...")

if __name__ == '__main__':
    notifier = Notifier()
    notifier.suscribe()