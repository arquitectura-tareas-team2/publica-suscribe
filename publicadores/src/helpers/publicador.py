##!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: publicador.py
# Capitulo: Estilo Publica-Suscribe
# Autor(es): Perla Velasco & Yonathan Mtz. & Jorge Solís
# Actualizado por:
#                 - Ulises Enrique Huerta Elías
#                 - Juan Francisco Navarro Ambriz
#                 - Alexis Ultreras Sotelo
#                 - Reyna Esmeralda Sánchez Rodríguez
# Version: 4.0.0 Mayo 2023
# Descripción:
#
#   Este archivo define la conexión del publicador hacia el el distribuidor de mensajes
#
#   A continuación se describen los métodos que se implementaron en este archivo:
#
#                                             Métodos:
#           +------------------------+--------------------------+-----------------------+
#           |         Nombre         |        Parámetros        |        Función        |
#           +------------------------+--------------------------+-----------------------+
#           |        publish()       |  - queue: nombre de la   |  - publica el mensaje |
#           |                        |    ruta con la que se    |    en el distribuidor |
#           |                        |    vinculará el mensaje  |    de mensajes        |
#           |                        |    enviado               |                       |
#           |                        |  - data: mensaje que     |                       |
#           |                        |    será enviado          |                       |
#           +------------------------+--------------------------+-----------------------+
#
#-------------------------------------------------------------------------

import stomp

def publish(queue, data):
    conn = stomp.Connection([("localhost", 61613)])
    conn.connect("admin", "admin", wait=True)
    conn.send(queue,data)
    conn.disconnect()